/**
 * @class {ClaimSpiffWebView}
 */
export default class ClaimSpiffWebView {

     _partnerSaleInvoiceId:string;

     _partnerSaleInvoiceNumber:string;

     _fileUrl:string;

     _partnerSaleRegId:number;


    /**
     * @param {string} partnerSaleInvoiceId
     * @param {string} partnerSaleInvoiceNumber
     * @param {string} fileUrl
     * @param {number} partnerSaleRegId
     */
    constructor(partnerSaleInvoiceId:string,
                partnerSaleInvoiceNumber:string,
                fileUrl:string,
                partnerSaleRegId:number
    ) {

        if (!partnerSaleInvoiceId) {
            throw new TypeError('partnerSaleInvoiceId required');
        }
        this._partnerSaleInvoiceId = partnerSaleInvoiceId;

        if (!partnerSaleInvoiceNumber) {
            throw new TypeError('partnerSaleInvoiceNumber required');
        }
        this._partnerSaleInvoiceNumber = partnerSaleInvoiceNumber;

        if (!fileUrl) {
            throw new TypeError('fileUrl required');
        }
        this._fileUrl = fileUrl;

        if (!partnerSaleRegId) {
            throw new TypeError('partnerSaleRegId required');
        }
        this._partnerSaleRegId = partnerSaleRegId;

    }

    /**
     * @returns {string}
     */
     get partnerSaleInvoiceId():string {
        return this._partnerSaleInvoiceId;
    }

    /**
     * @returns {string}
     */
     get partnerSaleInvoiceNumber():string {
        return this._partnerSaleInvoiceNumber;
    }

    /**
     * @returns {string}
     */
     get fileUrl():string {
        return this._fileUrl;
    }

    /**
     * @returns {number}
     */
     get partnerSaleRegId():number {
        return this._partnerSaleRegId;
    }


    toJSON() {
        return {
            partnerSaleInvoiceId:this._partnerSaleInvoiceId,
            partnerSaleInvoiceNumber: this._partnerSaleInvoiceNumber,
            fileUrl: this._fileUrl,
            partnerSaleRegId: this._partnerSaleRegId
        };
    }
}