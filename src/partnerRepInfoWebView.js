/**
 * @class {PartnerRepInfoWebView}
 */
export default class PartnerRepInfoWebView {

     _userId:string;

     _firstName:string;

     _lastName:string;

     _isBankInfoExists:boolean;

     _isW9InfoExists:boolean;

     _isContactInfoExists:boolean;

    /**
     * @param {string} userId
     * @param {string} firstName
     * @param {string} lastName
     * @param {boolean} isBankInfoExists
     * @param {boolean} isW9InfoExists
     * @param {boolean} isContactInfoExists
     */
    constructor(userId:string,
                firstName:string,
                lastName:string,
                isBankInfoExists:boolean,
                isW9InfoExists:boolean,
                isContactInfoExists:boolean
    ) {

        if (!userId) {
            throw new TypeError('userId required');
        }
        this._userId = userId;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        this._isBankInfoExists = isBankInfoExists;

        this._isW9InfoExists = isW9InfoExists;

        this._isContactInfoExists = isContactInfoExists;

    }


    /**
     * @returns {string}
     */
     get userId():string {
        return this._userId;
    }

    /**
     * @returns {string}
     */
     get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
     get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {boolean}
     */
     get isBankInfoExists():boolean {
        return this._isBankInfoExists;
    }

    /**
     * @returns {boolean}
     */
     get isW9InfoExists():boolean {
        return this._isW9InfoExists;
    }

    /**
     * @returns {boolean}
     */
     get isContactInfoExists():boolean {
        return this._isContactInfoExists;
    }

    toJSON() {
        return {
            userId:this._userId,
            firstName: this._firstName,
            lastName: this._lastName,
            isBankInfoExists: this._isBankInfoExists,
            isW9InfoExists: this._isW9InfoExists,
            isContactInfoExists: this._isContactInfoExists
        };
    }
}