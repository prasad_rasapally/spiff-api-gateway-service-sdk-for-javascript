/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    url: 'https://dummy-url.com',
    firstName: 'firstName',
    lastName: 'lastName',
    userId: '00u5h8rhbtzPEtCH20h7',
    sap_vendor_number : '0000000000',
    spiffEntitlementId: 123,
    partnerSaleRegistrationId: 111,
    installDate: "5/1/2016",
    spiffAmount: 100,
    partnerRepUserId: "00u5atomih4WBuahq0h7",
    invoiceUrl: "http://www.test.com",
    invoiceNumber: "1111222222",
    facilityName: "facilityone",
    accountId: "001K000001H2Km2IAF",
    file: new Blob([3], {type: 'image/png'}),
    sellDate : '1/1/2016'
};